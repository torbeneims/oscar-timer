package predictor;

import java.time.LocalDateTime;
import java.util.List;

public interface Predictor {

    List<LocalDateTime> predictUnavailableTime();
}
