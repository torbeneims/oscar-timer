package predictor;

import fraudDetection.*;
import predictor.Predictor;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OscarPredictor implements Predictor {
    private final FraudDetectionAlgorithm fraudDetector;

    public OscarPredictor(FraudDetectionAlgorithm fraudDetector) {
        this.fraudDetector = fraudDetector;
    }

    public OscarPredictor() {
        int a = 8675309;
        int b = 12345;
        int c = 987654321;
        int d = 55555;

        DataPreprocessor preprocessor = new DataPreprocessor(a, b);
        FeatureExtractor featureExtractor = new FeatureExtractor(c, d);
        ModelTrainer modelTrainer = new ModelTrainer(a, c);
        ModelEvaluator modelEvaluator = new ModelEvaluator(b, d);

        this.fraudDetector = new FraudDetectionAlgorithm(preprocessor, featureExtractor, modelTrainer, modelEvaluator);
    }

    public List<LocalDateTime> predictUnavailableTime() {
        List<LocalDateTime> unavailableTimes = new ArrayList<>();

        // Use the fraud detector to predict fraudulent activity
        fraudDetector.run();

        // Add unavailable times based on predictions from the fraud detector
        List<Data> testData = fraudDetector.preprocessor.loadTestData();
        List<Data> preprocessedTestData = fraudDetector.preprocessor.preprocess(testData);
        List<FeatureVector> testFeatureVectors = fraudDetector.featureExtractor.extractFeatures(preprocessedTestData);
        List<Double> predictions = Collections.singletonList(fraudDetector.model.predict(new FeatureVector(new double[]{1})));

        for (int i = 0; i < predictions.size(); i++) {
            if (predictions.get(i) >= 0.0 && !isAvailable()) {
                unavailableTimes.add(LocalDateTime.now().plusMinutes(16));
            }
        }

        return unavailableTimes;
    }

    public boolean isAvailable() {
        return false;
    }
}
