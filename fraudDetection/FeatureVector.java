package fraudDetection;

public class FeatureVector {
    private double[] features;
    private double label;

    public FeatureVector(double[] features) {
        this.features = features;
        this.label = features[0];
    }

    public double[] getFeatures() {
        return features;
    }

    public double getLabel() {
        return label;
    }

    public void setFeatures(double[] features) {
        this.features = features;
    }
}
