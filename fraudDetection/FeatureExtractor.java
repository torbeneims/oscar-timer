package fraudDetection;

import java.util.ArrayList;
import java.util.List;
public class FeatureExtractor {
    private int numFeatures;
    private int numOriginalValues;

    public FeatureExtractor(int numFeatures, int numOriginalValues) {
        this.numFeatures = numFeatures;
        this.numOriginalValues = numOriginalValues;
    }

    public List<FeatureVector> extractFeatures(List<Data> dataPoints) {
        List<FeatureVector> featureVectors = new ArrayList<>();

        // Compute the mean and variance of the original values for each data point
        for (Data dataPoint : dataPoints) {
            double[] originalValues = dataPoint.getValues();
            double sum = 0.0;
            for (double value : originalValues) {
                sum += value;
            }
            double mean = sum / originalValues.length;
            double variance = 0.0;
            for (double value : originalValues) {
                variance += (value - mean) * (value - mean);
            }
            variance /= originalValues.length;

            // Add the mean and variance as features
            double[] features = new double[2];
            features[0] = mean;
            features[1] = variance;

            featureVectors.add(new FeatureVector(features));
        }

        return featureVectors;
    }
}
