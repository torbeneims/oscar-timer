package fraudDetection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DataPreprocessor {
    private int seed;
    private int numFeatures;

    public DataPreprocessor(int seed, int numFeatures) {
        this.seed = seed;
        this.numFeatures = numFeatures;
    }

    public List<Data> loadData() {
        // Replace this with your own code to load data from a file or database
        List<Data> rawData = new ArrayList<>();
        rawData.add(new Data(new double[] {1.0, 2.0, 3.0}));
        rawData.add(new Data(new double[] {4.0, 5.0, 6.0}));
        rawData.add(new Data(new double[] {7.0, 8.0, 9.0}));
        return rawData;
    }

    public List<Data> loadTestData() {
        // load test data from a file or database
        List<Data> testData = new ArrayList<>();
        testData.add(new Data(Arrays.asList(3.0, 1.0, 4.0, 2.0), true));
        testData.add(new Data(Arrays.asList(1.0, 2.0, 3.0, 4.0), false));
        testData.add(new Data(Arrays.asList(2.0, 3.0, 1.0, 4.0), true));
        return testData;
    }


    public List<Data> preprocess(List<Data> rawData) {
        Random random = new Random(seed);
        List<Data> preprocessedData = new ArrayList<>();

        for (Data dataPoint : rawData) {
            double[] originalValues = dataPoint.getValues();

            // Add some noise to the data
            double[] noisyValues = new double[originalValues.length];
            for (int i = 0; i < originalValues.length; i++) {
                noisyValues[i] = originalValues[i] + random.nextGaussian();
            }

            // Add some additional features to the data
            double[] newValues = new double[originalValues.length + numFeatures];
            System.arraycopy(noisyValues, 0, newValues, 0, noisyValues.length);
            for (int i = 0; i < numFeatures; i++) {
                newValues[noisyValues.length + i] = random.nextDouble();
            }

            Data newDatapoint = new Data(newValues);
            preprocessedData.add(newDatapoint);
        }

        return preprocessedData;
    }
}
