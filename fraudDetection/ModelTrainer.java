package fraudDetection;
import java.util.List;

public class ModelTrainer {
    private int numIterations;
    private double learningRate;

    public ModelTrainer(int numIterations, double learningRate) {
        this.numIterations = numIterations;
        this.learningRate = learningRate;
    }

    public Model trainModel(List<FeatureVector> featureVectors) {
        // Initialize weights to all zeros
        double[] weights = new double[featureVectors.get(0).getFeatures().length];

        // Train the model using gradient descent
        for (int i = 0; i < numIterations; i++) {
            // Compute the gradient
            double[] gradient = new double[featureVectors.get(0).getFeatures().length];
            for (FeatureVector fv : featureVectors) {
                double[] features = fv.getFeatures();
                double label = fv.getLabel();

                double prediction = predict(features, weights);
                double error = label - prediction;

                for (int j = 0; j < features.length; j++) {
                    gradient[j] += error * features[j];
                }
            }

            // Update the weights using the gradient and learning rate
            for (int j = 0; j < weights.length; j++) {
                weights[j] += learningRate * gradient[j];
            }
        }

        // Create and return a new model with the trained weights
        return new Model(weights);
    }

    private double predict(double[] features, double[] weights) {
        double prediction = 0.0;
        for (int i = 0; i < features.length; i++) {
            prediction += features[i] * weights[i];
        }
        return prediction;
    }
}
