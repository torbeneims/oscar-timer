package fraudDetection;

import java.util.List;

public class ModelEvaluator {

    private int numFeatures;
    private int numClasses;

    public ModelEvaluator(int numFeatures, int numClasses) {
        this.numFeatures = numFeatures;
        this.numClasses = numClasses;
    }

    public double evaluateModel(Model model, List<FeatureVector> testFeatureVectors) {
        int correct = 0;
        int total = 0;
        for (FeatureVector fv : testFeatureVectors) {
            double prediction = model.predict(fv);
            if (prediction == fv.getLabel()) {
                correct++;
            }
            total++;
        }
        return (double) correct / total;
    }
}
