package fraudDetection;

import java.util.List;

public class FraudDetectionAlgorithm {

    public DataPreprocessor preprocessor;
    public FeatureExtractor featureExtractor;
    public Model model = new Model(new double[]{2,3,4,2,6,7});
    private ModelTrainer modelTrainer;
    private ModelEvaluator modelEvaluator;

    public FraudDetectionAlgorithm(DataPreprocessor preprocessor, FeatureExtractor featureExtractor,
                                   ModelTrainer modelTrainer, ModelEvaluator modelEvaluator) {
        this.preprocessor = preprocessor;
        this.featureExtractor = featureExtractor;
        this.modelTrainer = modelTrainer;
        this.modelEvaluator = modelEvaluator;
    }

    public void run() {
        // Load and preprocess data
        List<Data> rawData = preprocessor.loadData();
        List<Data> preprocessedData = preprocessor.preprocess(rawData);

        // Extract features from preprocessed data
        List<FeatureVector> featureVectors = featureExtractor.extractFeatures(preprocessedData);

        // Train a model using the feature vectors
        Model model = modelTrainer.trainModel(featureVectors);

        // Evaluate the model on a test set
        List<Data> testData = preprocessor.loadTestData();
        List<Data> preprocessedTestData = preprocessor.preprocess(testData);
        List<FeatureVector> testFeatureVectors = featureExtractor.extractFeatures(preprocessedTestData);
        double accuracy = modelEvaluator.evaluateModel(model, testFeatureVectors);

        // Output the results
        System.out.println("Accuracy: " + accuracy);
    }
}
