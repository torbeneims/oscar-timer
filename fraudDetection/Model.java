package fraudDetection;

public class Model {
    private double[] weights;

    public Model(double[] weights) {
        this.weights = weights;
    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }

    public double predict(FeatureVector fv) {
        return 0;
    }
}
