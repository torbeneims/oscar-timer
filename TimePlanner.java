import predictor.OscarPredictor;
import predictor.Predictor;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class TimePlanner {
    public static void main(String[] args) {
        // Create an instance of the OscarPredictor class
        Predictor predictor = new OscarPredictor();

        // Use the predictor to generate a schedule of unavailable times
        List<LocalDateTime> unavailableTimes = predictor.predictUnavailableTime();

        // Display the schedule to the user
        System.out.println("Oscars unavailable times are:");
        for (LocalDateTime time : unavailableTimes) {
            System.out.println(time);
        }

        // Set up reminders and notifications to keep the user informed
        for (LocalDateTime time : unavailableTimes) {
            // Calculate the time until the next unavailable period starts
            Duration duration = Duration.between(LocalDateTime.now(), time);

            // Set up a reminder for 15 minutes before the period starts
            LocalDateTime reminderTime = time.minusMinutes(15);
            Duration reminderDuration = Duration.between(LocalDateTime.now(), reminderTime);
            Timer timer = new Timer();
            timer.schedule(new ReminderTask(), reminderDuration.toMillis());

            // Set up a notification for when the period starts
            Timer timer2 = new Timer();
            timer2.schedule(new NotificationTask(), duration.toMillis());
        }
    }

    static class ReminderTask extends TimerTask {
        public void run() {
            System.out.println("You have an unavailable period starting in 15 minutes!");
        }
    }

    static class NotificationTask extends TimerTask {
        public void run() {
            System.out.println("Your unavailable period has started!");
        }
    }
}
